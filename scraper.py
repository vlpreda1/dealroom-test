import requests
import pandas as pd
from bs4 import BeautifulSoup
from helper import *


def crawl(url):
    """
    Crawler speciffically made for crawling the 
    'https://www.ycombinator.com/companies/' URL

    Input:
          url : String
    Output:
          List of Strings
    """
    try:
        page = requests.get(url)
        soup = BeautifulSoup(page.text, 'html.parser')
        main_info = soup.find_all("div", "main-box")
        side_info = soup.find_all("div", "highlight-box")

        title = soup.find_all('h1', "heavy")[0].text
        short_description = soup.find_all("h3")[0].text
        long_description = main_info[0].find('p', class_='pre-line').text
        link = main_info[0].find('a').text

        founded = side_info[0].find_all('div')[2].find('span',
                                                       class_='right').text
        team_size = side_info[0].find_all('div')[3].find('span',
                                                         class_='right').text
        location = side_info[0].find_all('div')[4].find('span',
                                                        class_='right').text
    except Exception:
        print("Encountered an error when parsing the website:\n{}".format(url))
        return ['', '', '', '', '', '', '']
    return [
        title, short_description, long_description, founded, team_size,
        location, link
    ]


def main():
    url = "https://www.ycombinator.com/companies/{}"
    c_mat = []
    for i in range(100, 1996):
        c_mat.append(crawl(url.format(i)))
        print("Finished scraping URL number {}".format(i - 99))

    c_mat = pd.DataFrame(c_mat,
                         columns=[
                             "Title", "Short", "Long", "Date", "Team Size",
                             "Location", "Website"
                         ])

    c_mat = c_mat.dropna(how="all")
    append_to_excel(c_mat,
                    "./Data_Science_Internship_Assignment.xlsx",
                    sheet='Scraping results')


if __name__ == "__main__":
    main()
