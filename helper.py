import pandas as pd
import numpy as np
from time import time
global test_frame
global tags_series
global labeled_tags


def load_data():
    t0 = time()
    data_frame = pd.read_excel("./Data_Science_Internship_Assignment.xlsx",
                               sheet_name='Data')
    first_series = pd.Series(
        data_frame['TAGS'].dropna().str.split(";").explode(), name="TAGS")
    first_series = first_series.value_counts().loc[lambda x: x > 9].keys()
    # Helper csv to store our hand labels
    labels = pd.read_csv("./tag_dict.csv", header=0)
    print("All files loaded successfully in:\t {} seconds".format(time() - t0))
    return [data_frame, first_series, labels]


def tag_labeler(ser, tag_dict):
    """
    Helper function to help hand label all elements in the TAGS column

    Input: ser -> Pandas Series
    Output: tag_dict -> Dictionary
        tag_dict contains every unique element in TAGS as keys
        and the hand labeled Categories as values
    """
    labeled = tag_dict["TAGS"].to_list()
    # Splits all values in ser separated by ";" and ignores duplicates
    # Also remove the already labeled elements in tag_dict.csv
    ser = ser.to_list()
    ser = [i for i in ser if i not in labeled]
    n_unique = len(ser)

    # in case tag_dict.csv is already populated
    if n_unique == 0:
        print("All the tags are already labeled in ./tag_dict.csv")
        return tag_dict

    c = 0

    print("\t\tWelcome to the hand labeler!\n\n")
    while (c < n_unique):
        # make use of already labeled information
        if ser[c] in tag_dict["TAGS"]:
            if c == n_unique - 1:
                tag_dict.to_csv("./tag_dict.csv",
                                columns=["TAGS", "VALUES"],
                                index=False,
                                header=True)
                return tag_dict
            else:
                c += 1
                continue
        else:
            print(
                "Which label should {} have?\t({} remaining)\n".format(
                    ser[c], n_unique - c), "\t1) Startups\n",
                "\t2) Mature Companies\n", "\t3) Universities/School\n",
                "\t4) Government/Non-profit\n", "\t5) Unclassified",
                "\t6) Save and Quit")
            # Catch unsuitable inputs
            try:
                choice = input()
                if choice == '1':
                    print('Choice : 1')
                    tag_dict = tag_dict.append(
                        {
                            "TAGS": ser[c],
                            "VALUES": "Startups"
                        },
                        ignore_index=True)
                elif choice == '2':
                    print('Choice : 2')
                    tag_dict = tag_dict.append(
                        {
                            "TAGS": ser[c],
                            "VALUES": "Mature Companies"
                        },
                        ignore_index=True)
                elif choice == '3':
                    print('Choice : 3')
                    tag_dict = tag_dict.append(
                        {
                            "TAGS": ser[c],
                            "VALUES": "Universities/School"
                        },
                        ignore_index=True)
                elif choice == '4':
                    print('Choice : 4')
                    tag_dict = tag_dict.append(
                        {
                            "TAGS": ser[c],
                            "VALUES": "Government/Non-profit"
                        },
                        ignore_index=True)
                elif choice == '5':
                    print('Choice : 5')
                    tag_dict = tag_dict.append(
                        {
                            "TAGS": ser[c],
                            "VALUES": "Unclassified"
                        },
                        ignore_index=True)

                elif choice == '6':
                    print('choice : 6')
                    tag_dict.to_csv("./tag_dict.csv",
                                    columns=["TAGS", "VALUES"],
                                    index=False,
                                    header=True)
                    return tag_dict
            except Exception:
                print("Your choice must be a digit!")
                continue

            c += 1

    tag_dict.to_csv("./tag_dict.csv",
                    columns=["TAGS", "VALUES"],
                    index=False,
                    header=True)
    return


def show_hand_labels(dic):
    print(dic)


def tag_score(tags, df):
    """
    This function computes a scoring vector with a value for each labels
    based on its TAGS column

    Input:
           tags : String
           df   : DataFrame (of labeled tags)
    Output:
          score : List of floats
    """
    score = np.zeros(5)
    if type(tags) != str:
        return score

    tags = tags.split(";")
    n = len(tags)

    for i in tags:
        # check that 'i' is in the DataFrame
        existance_checker = (df.TAGS == i).any()
        # any(c['TAGS'] == 'financial')
        # df[df['TAGS'] == i].TAGS.values[0]
        # df[df['TAGS'] == i]
        # (df[df['TAGS'] == i]).bool()
        # df['TAGS'].str.contains(i).any()
        if existance_checker:
            label = df[df['TAGS'] == i].VALUES.values[0]
            # df[df['TAGS'] == i].values[0, 1]
            # df.loc[df[df.TAGS == i].index[0], 'VALUES']
            if label == "Startups":
                score[0] += 1
            elif label == "Mature Companies":
                score[1] += 1
            elif label == "Universities/School":
                score[2] += 1
            elif label == "Government/Non-profit":
                score[3] += 1
            else:
                score[4] += 1
        else:
            n -= 1
            continue
    score = score / n
    return score


def classifier(probs, l_data):
    """
    This pipe will be applied to all entries in the DataFrame using
    DataFrame.apply and will determine what to write in the TYPE column

    Input:
          probs           : List of "probabilities"
          l_data          : List of strings
              date        : String whose first 4 chars represent a year
              g_stage     : String

    Output:
          type_of_company : String (one of the four classes)
    """
    date, g_stage = l_data
    if type(date) == str:
        date = int(date[:4])

    if probs[3] >= 0.3:
        type_of_company = "Government/Non-profit"
    elif probs[2] >= 0.2:
        type_of_company = "Universities/School"
    elif probs[1] > probs[0] and date < 1990 and (g_stage != 'seed stage'):
        type_of_company = "Mature Companies"
    elif probs[0] > probs[1] and date >= 1990 and (g_stage !=
                                                   'late growth stage'):
        type_of_company = "Startups"
    else:
        type_of_company = "Unclassified"
    return type_of_company


def append_to_excel(df, file_name, sheet='new_sheet', **kwargs):
    t0 = time()
    with pd.ExcelWriter(file_name, mode='a') as writer:
        df.to_excel(writer, **kwargs, sheet_name=sheet, header=None)
    writer.close()
    print(
        'The DataFrame of size:\n {} KB \nwas written to Excel in {} seconds'.
        format(df.memory_usage(),
               time() - t0))

    return
