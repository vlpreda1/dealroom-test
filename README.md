
# Table of Contents

1.  [What is this repository for?](#org9d705bc)
    1.  [Quick summary](#orgc40d662)
2.  [How do I get set up?](#org200279a)
    1.  [Summary of set up](#orgf6f5066)
    2.  [Configuration](#orge9e7e87)
    3.  [How to run the code](#org8d13a62)
        1.  [file:main.py](#org414d87a)
        2.  [file:scraper.py](#org3585d6c)
        3.  [file:helper.py](#orgd403c68)
3.  [The Classifier](#org453079e)




# What is this repository for?



## Quick summary

This repo is an entry internship assignment. The description of the tasks
are found on the first sheet of the <Data_Science_Internship_Assignment.xlsx>



# How do I get set up?



## Summary of set up

Make sure python3 and pip3 are installed on your device



## Configuration

To install all the necessary libraries 

    cd /path/to/repo/
    pip3 install -r requirements.txt



## How to run the code



### <main.py>

The file main.py is a small interface where you can hand label the unique
entries in tags that appear more than 9 (number chosen arbitrarely) times.

In this interface you can also have a look at the tags that are already
labeled and classify the entries into one of `Startups`, `Mature
    Companies`, `Governmental/Non-profit`, `Universities/School`. The result is
written to the original `.xlsx` file.

To run it, type in terminal:

    python3 main.py



### <scraper.py>

This file crawls the <https://www.ycombinator.com/companies/> domain and
writes to the original `.xlsx` file.

To run it, type in terminal:

    python3 scraper.py



### <helper.py>

Contains useful helper function used in this project. This code producen no
output



# The Classifier

Since the original dataset is relatively small for most machine learning
algorithms to be acceptable in terms of efficiency, I decided to bese my
labeling on a logical scheme.

In order to do that I had to hand label all the unique `TAGS`. Since there
were over 2000 of them, the entries were filtered based on how often they
appear in the entire dataset. Tags that appeared less than 9 times were
discarded. The remaining labels were around 500, which was much more
manageable.

In the classification process, arbitrary weights were assigned to the final
labels since `Universities/School` and `Governmental/Non-profit` appeared
more rarely than the others, during the hand labeling process. These weights
can be chosen better based on further data analysis. The date condition given
to `Startups` and `Mature Companies` is strictly enforced and `early growth
  stage` is considered neutral, while the other two further contribute to the
classification of `Startups` and `Mature Companies`

