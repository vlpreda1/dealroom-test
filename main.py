from helper import *
import pandas as pd
from time import time


def main():


    test_frame, tags_series, labeled_tags = load_data()


    while True:
        print("Welcome to the app! Please tell us what you want to do: \n\n",
              "\t1) Classify the data\n", "\t2) Label each TAG\n",
              "\t3) See your labels\n", "\t4) Exit\n")
        try:
            choice = input()
            if choice == '1':
                t0 = time()

                # Compute a 'score' based on the TAGS corresponding to each
                # row in the DataFrame

                score_vector = [
                    tag_score(i, labeled_tags) for i in test_frame.TAGS
                ]

                print("Label scores were computed in:\t{} seconds\n".format(
                    time() - t0))
                t0 = time()

                # Classify all rows and assign them to the main DataFrame

                test_frame['TYPE'] = [
                    classifier(
                        score_vector[i],
                        test_frame[['TAGS', 'LAUNCH DATE',
                                    'GROWTH STAGE']].iloc[i].values[1:])
                    for i in range(test_frame.shape[0])
                ]

                print(
                    "The classification process was computed in:\t{} seconds\n"
                    .format(time() - t0))

                # Write the new labels to the original Excel
                # Note: Can't figure out how to write to a specific column in
                # an excel file. Thus, this creates a new Sheet containing the
                # classified labels

                append_to_excel(
                    test_frame['TYPE'],
                    './Data_Science_Internship_Assignment.xlsx',
                    sheet='Data',
                )

                print("The class distribution over the data is: \n\n",
                      test_frame['TYPE'].value_counts())

                return
            elif choice == '2':

                # Calls the function that allows you to hand label TAGS from
                # the Data
                labeled_tags = tag_labeler(tags_series, labeled_tags)

            elif choice == '3':

                # Displays the already labeled entries
                show_hand_labels(labeled_tags)

            elif choice == '4':
                return
        except Exception:
            print("Your choice must be ONE digit!")
            continue

    return


if __name__ == "__main__":
    main()
